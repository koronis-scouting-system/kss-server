const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

exports.user = require('./user');
exports.verify = require('./verify');

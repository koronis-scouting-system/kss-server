const functions = require('firebase-functions');
const secret = require('@mochen/secret');
const admin = require('firebase-admin');

var db = admin.database();
var secretsRef = db.ref('secrets');

exports.addSecretOnCreate = functions.auth.user().onCreate(async (userRecord) => {
  var snapshot = await secretsRef.child(userRecord.uid).once('value');
  var userSecret = '';
  if(snapshot.exists()) {
    userSecret = snapshot.val();
  }
  else {
    userSecret = secret();
    await secretsRef.child(userRecord.uid).set(userSecret);
  }
  return userRecord;
});

exports.addSecretOnOperation = functions.auth.user().onDelete(async (userRecord) => {
  var snapshot = await secretsRef.child(userRecord.uid).once('value');
  if(snapshot.exists()) {
    await snapshot.remove();
  }
  return userRecord;
});

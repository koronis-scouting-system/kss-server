const functions = require('firebase-functions');
const admin = require('firebase-admin');
var CryptoJS = require('crypto-js');

var db = admin.database();
var secretsRef = db.ref('secrets');

exports.verifyProcess = functions.https.onCall(async (data, context) => {
  var process = data;
  var inUserId = process.user;
  if(inUserId === '') {return {isValid: false}}
  var inYear = process.year;
  var inQueryType = process.queryType;
  var inDataType = process.dataType;
  var inName = process.name;
  var inTitle = process.title;
  var inDescription = process.description;
  var inFunction = process.function;
  var hash = CryptoJS.SHA3(inUserId + String(inYear) + inQueryType +
    inDataType + inName + inTitle + inDescription + inFunction +
    (await secretsRef.child(inUserId).once('value'))
  ).toString(CryptoJS.enc.Base64);
  var isValid = process.digitalSignature === hash;
  return {isValid: isValid};
});

exports.verifyRecord = functions.https.onCall(async (data, context) => {
  var record = data;
  var inUserId = record.user;
  if(inUserId === '') {return {isValid: false}}
  var inYear = record.year;
  var inVersion = record.version;
  var inMatchStartDate = record.startDate;
  var inMatchNumber = record.matchNumber;
  var inMatchType = record.matchType;
  var inTeamNumber = record.teamNumber;
  var inEventLog = record.eventLog;
  var inPositionLog = record.positionLog;
  var hash = CryptoJS.SHA3(inUserId + String(inYear) + String(inVersion) + String(inMatchStartDate) + String(inMatchNumber) + inMatchType + inTeamNumber +
    JSON.stringify(inEventLog.sort((e1, e2) => {return e1.timeStamp - e2.timeStamp})) +
    JSON.stringify(inPositionLog.sort((e1, e2) => {return e1.timeStamp - e2.timeStamp})) +
    (await secretsRef.child(inUserId).once('value'))
  ).toString(CryptoJS.enc.Base64);
  var isValid = record.digitalSignature === hash;
  return {isValid: isValid};
});
